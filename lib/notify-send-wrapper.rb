module Notify
  ICONS = {updates: "/usr/share/icons/gnome/32x32/apps/update-manager.png",
          battery: "/usr/share/icons/gnome/32x32/status/battery-empty.png",
          network: "/usr/share/icons/gnome/32x32/places/gtk-network.png",
          meteo: "/usr/share/icons/gnome/32x32/status/stock_weather-few-clouds.png",
          package: "/usr/share/icons/gnome/32x32/mimetypes/package.png",
          calendar: "/usr/share/icons/gnome/32x32/mimetypes/stock_calendar.png"}
  URGENCY = [:low, :normal, :critical]
  DEBUG = true

  def self.send text, opts={}
    cmd = "notify-send"
    if opts[:icon]
      if opts[:icon].to_s.include? "/"
        cmd += " -i #{opts[:icon]}"
      else
        cmd += " -i #{Notify::ICONS[opts[:icon]]}"
      end
    end

    cmd += "-c #{opts[:category]}" if opts[:category]

    #opts[:time] ||= 5000
    cmd += " -t #{opts[:time]}" if opts[:time]
    
    opts[:urgency] = URGENCY[opts[:urgency]] if opts[:urgency].class == Numeric
    cmd += " -u #{opts[:urgency].to_s}" if opts[:urgency]

    if opts[:summary]
      cmd += " \"#{opts[:summary]}\"" 
    else
      cmd += ""
    end
    cmd += " \"#{text}\""
    puts cmd if DEBUG
    system cmd
  end
end

