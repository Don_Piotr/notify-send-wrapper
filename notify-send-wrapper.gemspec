Gem::Specification.new do |s|
  s.name        = 'notify-send-wrapper'
  s.version     = '0.1.8'
  s.date        = '2022-05-23'
  s.summary     = "Simple API for libnotify"
  s.authors     = ["Don Piotr Talarczyk"]
  s.email       = 'don.piotr@netc.it'
  s.files       = ["lib/notify-send-wrapper.rb"]
  s.homepage      = "https://bitbucket.org/Don_Piotr/"
  s.license       = "MIT"
end
