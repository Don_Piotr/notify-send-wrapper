# notify-send-wrapper

## Requirments
- libnotify
- gnome icons

## Use
```ruby
require 'notify-send-wrapper'

Notify.send("Messaggio", {icon: :package, time: 3000, urgency: 2, summary: "Sommario"})
```
### Icons
- :updates
- :battery
- :network
- :meteo
- :package
- :calendar
- path to icon

### Urgency
- 0 or "low"
- 1 or "normal"
- 2 or "critical"

